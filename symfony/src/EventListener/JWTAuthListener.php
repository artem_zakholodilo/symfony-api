<?php

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class JWTAuthListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface\
     */
    private $tokenStorage;

    /**
     * @var AuthenticationManagerInterface
     */
    private $authManager;

    private $providerKey;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * JWTAuthListener constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authManager
     * @param ContainerInterface $container
     */
    public function __construct(
        TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authManager,
        ContainerInterface $container
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authManager = $authManager;
        $this->container = $container;
    }

    /**
     * @param RequestEvent $event
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->headers->has('Authorization')) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The Security Bundle is not registered in your application.');
        }
        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }
        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

    }
}