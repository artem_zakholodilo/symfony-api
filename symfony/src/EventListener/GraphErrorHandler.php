<?php

namespace App\EventListener;

use GraphQL\Error\Error;
use GraphQL\Error\FormattedError;
use Overblog\GraphQLBundle\Event\ExecutorResultEvent;

class GraphErrorHandler
{
    public function onPostExecutor(ExecutorResultEvent $event)
    {
        $myErrorFormatter = function (Error $error) {
            return FormattedError::createFromException($error);
        };

        $myErrorHandler = function (array $errors, callable $formatter) {
            return array_map($formatter, $errors);
        };

        $event->getResult()
            ->setErrorFormatter($myErrorFormatter)
            ->setErrorsHandler($myErrorHandler);
    }
}