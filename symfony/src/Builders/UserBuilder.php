<?php


namespace App\Builders;

use App\Entity\User;

class UserBuilder
{
    /**
     * @var User
     */
    private $user;

    public function __construct()
    {
    }

    public function create(): self
    {
        $this->user = new User();
        return $this;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function withUsername(string $username): self
    {
        $this->user->setUsername($username);
        return $this;
    }

    public function withEmail(string $email): self
    {
        $this->user->setEmail($email);
        return $this;
    }

    public function withPassword(string $password): self
    {
        $this->user->setPassword($password);
        return $this;
    }

    public function withValidationToken(string $validationToken): self
    {
        $this->user->setValidationToken($validationToken);
        return $this;
    }

    public function withResetToken(string $resetToken): self
    {
        $this->user->setResetToken($resetToken);
        return $this;
    }

    public function build(): User
    {
        return $this->user;
    }
}
