<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="\App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="user.exists")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    public const DEFAULT_ITEMS_PER_PAGE = 10;

    public const TYPE_USER = 1;

    public const TYPE_AUTHOR = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    protected $posts;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     */
    private $comments;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="type", options={"default": 1})
     */
    private $type;

    /**
     * @ORM\Column(type="uuid", unique=true, nullable=true)
     */
    private $resetToken;

    /**
     * @var Ramsey\Uuid\UuidInterface
     * @ORM\Column(type="uuid", unique=true, nullable=true)
     */
    private $validationToken;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = static::TYPE_USER;
        $this->posts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function setPosts(Post $post): void
    {
        $this->posts[] = $post;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     */
    public function setComment(Comment $comment): void
    {
        $this->comments[] = $comment;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function getValidationToken(): ?string
    {
        return $this->validationToken;
    }
    public function setValidationToken(string $validationToken)
    {
        $this->validationToken = $validationToken;
    }

    public function getResetToken(): ?string
    {
        return $this->resetToken;
    }
    public function setResetToken(string $resetToken)
    {
        $this->resetToken = $resetToken;
    }
}
