<?php

namespace App\GraphQL\Resolver;

use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Overblog\GraphQLBundle\Relay\Connection\ConnectionInterface;
use Overblog\GraphQLBundle\Relay\Connection\Output\Connection;
use Overblog\GraphQLBundle\Relay\Connection\Paginator;

Class AuthorResolver implements ResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * AuthorResolver constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ResolveInfo $info
     * @param $value
     * @param Argument $args
     * @return mixed
     */
    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    /**
     * @param string $id
     * @return User|object
     */
    public function resolve($id): User
    {
        return $this->em->find(User::class, $id);
    }

    /**
     * @param Argument $args
     * @return mixed
     */
    public function resolveAll($args)
    {
        $authors = $this->em->find(User::class, ['type' => User::TYPE_AUTHOR]);
        $paginator = new Paginator(function ($offset, $limit) use ($authors) {
            return $authors->slice($offset, $limit ?? User::DEFAULT_ITEMS_PER_PAGE);
        });
        return $paginator->auto($args, count($authors));
    }

    /**
     * @param User $author
     * @return int|null
     */
    public function id(User $author): ?int
    {
        return $author->getId();
    }

    /**
     * @param User $author
     * @return string
     */
    public function email(User $author): string
    {
        return $author->getEmail();
    }

    /**
     * @param User $author
     * @param Argument $args
     * @return ConnectionInterface
     */
    public function posts(User $author, Argument $args)
    {
        $posts = $author->getPosts();
        $paginator = new Paginator(function ($offset, $limit) use ($posts) {
            return $posts->slice($offset, $limit ?? Post::DEFAULT_PAGE_VALUE);
        });
        return $paginator->auto($args, count($posts));
    }

    /**
     * @param User $user
     * @param Argument $args
     * @return object|Connection
     */
    public function comments(User $user, Argument $args)
    {
        $comments = $user->getComments();
        $paginator = new Paginator(function ($offset, $limit) use ($comments) {
            return $comments->slice($offset, $limit ?? Comment::DEFAULT_PAGE_VALUE);
        });
        return $paginator->auto($args, count($comments));
    }
}