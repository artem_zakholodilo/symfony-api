<?php

namespace App\GraphQL\Resolver;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class CommentResolver implements ResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * AuthorResolver constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ResolveInfo $info
     * @param $value
     * @param Argument $args
     * @return mixed
     */
    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    /**
     * @param string $id
     * @return Post|object
     */
    public function resolve(string $id): Post
    {
        return $this->em->find(Comment::class, $id);
    }

    /**
     * @param Comment $comment
     * @return int
     */
    public function id(Comment $comment): int
    {
        return $comment->getId();
    }

    /**
     * @param Comment $comment
     * @return string
     */
    public function body(Comment $comment): string
    {
        return $comment->getBody();
    }

    /**
     * @param Comment $comment
     * @return User
     */
    public function author(Comment $comment): User
    {
        return $comment->getAuthor();
    }

    /**
     * @param Comment $comment
     * @return Post
     */
    public function post(Comment $comment): Post
    {
        return $comment->getPost();
    }
}