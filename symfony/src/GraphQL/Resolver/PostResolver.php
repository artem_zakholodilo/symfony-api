<?php

namespace App\GraphQL\Resolver;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Overblog\GraphQLBundle\Relay\Connection\Paginator;

Class PostResolver implements ResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * AuthorResolver constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ResolveInfo $info
     * @param $value
     * @param Argument $args
     * @return mixed
     */
    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    /**
     * @param string $id
     * @return User|object
     */
    public function resolve($id): Post
    {
        return $this->em->find(Post::class, $id);
    }

    /**
     * @param Post $post
     * @return int
     */
    public function id(Post $post): int
    {
        return $post->getId();
    }

    /**
     * @param Post $post
     * @return string
     */
    public function title(Post $post): string
    {
        return $post->getTitle();
    }

    /**
     * @param Post $post
     * @return User
     */
    public function author(Post $post): User
    {
        return $post->getAuthor();
    }

    /**
     * @param Post $post
     * @param Argument $args
     * @return mixed
     */
    public function comments(Post $post, Argument $args)
    {
        $comments = $post->getComments();
        $paginator = new Paginator(function($offset, $limit) use($comments) {
            return $comments->sline($offset, $limit ?? Comment::DEFAULT_PAGE_VALUE);
        });

        return $paginator->auto($args, count($comments));
    }
}