<?php

namespace App\GraphQL\Input;

use \AssoConnect\GraphQLMutationValidatorBundle\Input\RequestObject;
use \AssoConnect\GraphQLMutationValidatorBundle\Validator\Constraints as AssoConnectAssert;

/**
 * @AssoConnectAssert\GraphQLRequestObject()
 */
class PostInput extends RequestObject
{
    public $id;

    /**
     * @see \App\Entity\Post::$title
     */
    public $title;

    public $body;

    public $author;

    public $rating;
}