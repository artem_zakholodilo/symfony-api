<?php

namespace App\GraphQL\Input;

use App\Entity\Comment;
use \AssoConnect\GraphQLMutationValidatorBundle\Input\RequestObject;
use \AssoConnect\GraphQLMutationValidatorBundle\Validator\Constraints as AssoConnectAssert;

/**
 * @AssoConnectAssert\GraphQLRequestObject()
 */
class CommentInput extends RequestObject
{
    public $className = Comment::class;

    public $id;

    public $body;

    public $author;

    public $post;
}