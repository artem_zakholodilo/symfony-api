<?php

namespace App\GraphQL\Input;
use \AssoConnect\GraphQLMutationValidatorBundle\Input\RequestObject;
use \AssoConnect\GraphQLMutationValidatorBundle\Validator\Constraints as AssoConnectAssert;

/**
 * @AssoConnectAssert\GraphQLRequestObject()
 */
class UserInput extends RequestObject
{
    public $id;

    public $email;

    public $username;

    public $password;

    public $type;
}