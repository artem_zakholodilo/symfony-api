<?php


namespace App\GraphQL\Mutation;

use App\Entity\Comment;
use App\Exceptions\GraphQLException;
use AssoConnect\GraphQLMutationValidatorBundle\Input\RequestObject;

/**
 * Trait DeleteMutation
 * @package App\GraphQL\Mutation
 */
trait DeleteMutation
{
    /**
     * @param RequestObject $input
     * @return int|null
     */
    protected function deleteField(RequestObject $input)
    {
        if ($this instanceof DataInterface) {
            $entity = $this->getEntityManger()->getRepository($input->className)->findOneBy($input->id);

            if (!$entity) {
                throw GraphQLException::fromString('bad_request');
            }
            $this->getEntityManger()->remove($entity);

            return $input->id;
        }

        return null;
    }
}
