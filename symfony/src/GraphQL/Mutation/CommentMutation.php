<?php

namespace App\GraphQL\Mutation;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use App\GraphQL\Input\CommentInput;
use AssoConnect\GraphQLMutationValidatorBundle\Exception\UserException;
use AssoConnect\GraphQLMutationValidatorBundle\Validator\MutationValidator;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class CommentMutation implements MutationInterface, AliasedInterface, DataInterface
{
    use DeleteMutation;
    /**
     * @EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @MutationValidator
     */
    protected $validator;

    /**
     * PostMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param MutationValidator $validator
     */
    public function __construct(EntityManagerInterface $entityManager, MutationValidator $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function getEntityManger()
    {
        return $this->entityManager;
    }

    /**
     * @param Argument $args
     * @return Comment
     * @throws UserException
     */
    public function create(Argument $args): Comment
    {
        $input = new CommentInput($args);
        $this->validator->validate($input);

        $comment = new Comment();
        $comment->setBody($input->body);
        [$author, $post] = $this->checkRequirements($input);
        $comment->setUser($author);
        $comment->setPost($post);

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return $comment;
    }

    /**
     * @param Argument $args
     * @return Comment
     * @throws UserException
     */
    public function update(Argument $args): Comment
    {
        $input = new CommentInput($args);
        $this->validator->validate($input);

        [$author, $post, $comment] = $this->checkRequirements($input);
        $comment->setBody($input->body);
        $comment->setUser($author);
        $comment->setPost($post);

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return $comment;
    }

    /**
     * @param Argument $args
     * @return int|null
     * @throws UserException
     */
    public function delete(Argument $args): ?int
    {
        $input = new CommentInput($args);
        $this->validator->validate($input);

        return $this->deleteField($input);
    }

    /**
     * Returns methods aliases.
     *
     * For instance:
     * array('myMethod' => 'myAlias')
     *
     * @return array
     */
    public static function getAliases()
    {
        return [
            'create' => 'create_comment',
            'update' => 'update_comment',
            'delete' => 'delete_comment'
        ];
    }

    /**
     * @param $input
     * @return array[User, Post, Comment]
     * @throws UserException
     */
    private function checkRequirements($input)
    {
        $author = $this->entityManager->getRepository(User::class)->findOneBy($input->author);
        $comment = $this->entityManager->getRepository(Comment::class)->findOneBy($input->id);
        $post = $this->entityManager->getRepository(Post::class)->findOneBy($input->post);
        if ((!$author || $author->getType() !== User::TYPE_AUTHOR) || !$post || !$comment) {
            throw new UserException(new ConstraintViolationList([]));
        }

        return [$author, $post, $comment];
    }
}
