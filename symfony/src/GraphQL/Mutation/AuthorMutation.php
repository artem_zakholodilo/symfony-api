<?php

namespace App\GraphQL\Mutation;

use App\Entity\Post;
use App\Entity\User;
use App\GraphQL\Input\CommentInput;
use App\GraphQL\Input\PostInput;
use App\GraphQL\Input\UserInput;
use AssoConnect\GraphQLMutationValidatorBundle\Exception\UserException;
use AssoConnect\GraphQLMutationValidatorBundle\Validator\MutationValidator;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class AuthorMutation implements MutationInterface, AliasedInterface, DataInterface
{
    use DeleteMutation;

    /**
     * @EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @MutationValidator
     */
    protected $validator;

    /**
     * PostMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param MutationValidator $validator
     */
    public function __construct(EntityManagerInterface $entityManager, MutationValidator $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function getEntityManger()
    {
        return $this->entityManager;
    }

    /**
     * @param Argument $args
     * @return User
     * @throws \Exception
     */
    public function create($args): User
    {
        $input = new UserInput($args);
        $this->validator->validate($input);

        $user = new User();
        $user->setUserName($input->username);
        $user->setEmail($input->email);
        $user->setPassword($input->password);

        $user->setType($input->type ?? User::TYPE_USER);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param $args
     * @return User|object
     * @throws UserException
     */
    public function update($args): User
    {
        $input = new UserInput($args);
        $this->validator->validate($input);

        $user = $this->checkRequired($input);
        $user->setUserName($input->username);
        $user->setEmail($input->email);
        $user->setPassword($input->password);

        $user->setType($input->type ?? User::TYPE_USER);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param Argument $args
     * @return int|null
     * @throws UserException
     */
    public function delete(Argument $args): ?int
    {
        $input = new UserInput($args);
        $this->validator->validate($input);

        return $this->deleteField($input);
    }

    /**
     * Returns methods aliases.
     *
     * For instance:
     * array('myMethod' => 'myAlias')
     *
     * @return array
     */
    public static function getAliases()
    {
        return [
            'create' => 'create_author',
            'update' => 'update_author',
            'delete' => 'delete_author'
        ];
    }

    /**
     * @param $input
     * @return User
     * @throws UserException
     */
    private function checkRequired($input)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy($input->author);
        if (!$user) {
            throw new UserException(new ConstraintViolationList([]));
        }

        return $user;
    }
}