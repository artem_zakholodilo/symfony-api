<?php

namespace App\GraphQL\Mutation;

use App\Entity\Post;
use App\Entity\User;
use App\GraphQL\Input\CommentInput;
use App\GraphQL\Input\PostInput;
use AssoConnect\GraphQLMutationValidatorBundle\Exception\UserException;
use AssoConnect\GraphQLMutationValidatorBundle\Validator\MutationValidator;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class PostMutation implements MutationInterface, AliasedInterface, DataInterface
{
    use DeleteMutation;

    /**
     * @EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @MutationValidator
     */
    protected MutationValidator $validator;

    /**
     * PostMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param MutationValidator $validator
     */
    public function __construct(EntityManagerInterface $entityManager, MutationValidator $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function getEntityManger()
    {
        return $this->entityManager;
    }

    /**
     * @param Argument $args
     * @return Post
     * @throws \Exception
     */
    public function create($args): Post
    {
        $input = new PostInput($args);
        $this->validator->validate($input);

        $post = new Post();
        $post->setTitle($input->title);
        $post->setBody($input->body);

        $author = $this->entityManager->getRepository(User::class)->findOneBy($input->author);
        if (!$author || $author->getType() !== User::TYPE_AUTHOR) {
            throw new UserException(new ConstraintViolationList([]));
        }
        $post->setAuthor($author);
        $post->setRating($input->rating ?? Post::DEFAULT_RATING);

        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return $post;
    }

    /**
     * @param $args
     * @return Post|object
     * @throws UserException
     */
    public function update($args): Post
    {
        $input = new PostInput($args);
        $this->validator->validate($input);

        [$author, $post] = $this->checkRequired($input);
        $post->setTitle($input->title);
        $post->setBody($input->body);
        $post->setAuthor($author);
        $post->setRating($input->rating ?? Post::DEFAULT_RATING);
        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return $post;
    }

    /**
     * @param Argument $args
     * @return int|null
     * @throws UserException
     */
    public function delete(Argument $args): ?int
    {
        $input = new PostInput($args);
        $this->validator->validate($input);

        return $this->deleteField($input);
    }

    /**
     * Returns methods aliases.
     *
     * For instance:
     * array('myMethod' => 'myAlias')
     *
     * @return array
     */
    public static function getAliases()
    {
        return [
            'create' => 'create_post',
            'update' => 'update_post',
            'delete' => 'delete_post'
        ];
    }

    /**
     * @param $input
     * @return array[User, Post]
     * @throws UserException
     */
    private function checkRequired($input)
    {
        $post = $this->entityManager->getRepository(Post::class)->findOneBy($input->id);
        $author = $this->entityManager->getRepository(User::class)->findOneBy($input->author);
        if ((!$author || $author->getType() !== User::TYPE_AUTHOR) || $post) {
            throw new UserException(new ConstraintViolationList([]));
        }

        return [$author, $post];
    }
}
