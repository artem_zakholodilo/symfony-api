<?php

namespace App\GraphQL\Mutation;

use Doctrine\ORM\EntityManagerInterface;

interface DataInterface
{
    /**
     * @return EntityManagerInterface
     */
    public function getEntityManger();
}