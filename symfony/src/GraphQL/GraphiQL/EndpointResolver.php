<?php

namespace App\GraphQL\GraphiQL;

class EndpointResolver
{
    public static function getByName($name)
    {
        if ('default' === $name) {
            return [
                'overblog_graphql_endpoint',
            ];
        }

        return [
            'graphql_default',
            ['schemaName' => $name],
        ];
    }
}
